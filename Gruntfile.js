module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        typescript: {
            base: {
                src: ['src/**/*.ts'],
                dest: 'js/src/index.min.js',
                options: {
                    module: 'amd',
                    target: 'es5'
                }
            }
        },
        uglify: {
            build: {
                src: 'js/src/**/*.js',
                dest: 'index.min.js'
            }
        }
    });
    grunt.loadNpmTasks('grunt-typescript');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('compile', ['typescript']);
};