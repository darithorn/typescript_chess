///<reference path="rect.ts"/>
///<reference path="globals.ts"/>
import * as Math from './rect';
import * as Globals from './globals';

export class Piece {
    hidden: boolean; 
    captured: boolean;    
    boardIndex: [number, number]; // integer index of 8x8 board
    firstMove: boolean = true;
    bounds: Math.Rect; /// contains placement in pixels
    light: boolean; /// true if white false if black
    private _img: HTMLImageElement;

    /// boardIndex: the integer index coordinate of 8x8 board (zero-indexed)
    /// pieceWidth: the width of the checker board sub-rectangle - usually SCREEN_WIDTH / 8
    /// pieceHeight: the height of the checker board sub-rectangle - usually SCREEN_HEIGHT / 8
    constructor(boardIndex: [number, number], imgID: string) {
        this.boardIndex = boardIndex;
        this.bounds = new Math.Rect(boardIndex[0] * Globals.PIECE_WIDTH, boardIndex[1] * Globals.PIECE_HEIGHT, Globals.PIECE_WIDTH, Globals.PIECE_HEIGHT);
        this._img = document.getElementById(imgID) as HTMLImageElement;
        this.light = imgID.search(/light/g) != -1;
        Globals.board[this.x][this.y] = this;
    }

    get x(): number { return this.boardIndex[0]; }
    set x(tmp: number) {
        this.boardIndex[0] = tmp;
        this.bounds.x = tmp * this.bounds.w;
    }
    get y(): number { return this.boardIndex[1]; }
    set y(tmp: number) {
        this.boardIndex[1] = tmp;
        this.bounds.y = tmp * this.bounds.h;
    }

    draw(context: CanvasRenderingContext2D) {
        context.drawImage(this._img, this.x * this.bounds.w, this.y * this.bounds.h, this.bounds.w, this.bounds.h);
    }

    drawPixel(context: CanvasRenderingContext2D) {
        context.drawImage(this._img, this.bounds.x, this.bounds.y, this.bounds.w, this.bounds.h);        
    }

    getAvailableMoves(): Array<[number, number]> {
        /// result indices should be absolute and not relative to the piece
        var result = [];
        let dir = this.light ? -1 : 1;
        var piece: Piece;
        /// default is available moves for pawn
        if(this.firstMove && Globals.getPieceByLocation(this.x ,this.y + (2 * dir)) == null) {
            result.push([this.x, this.y + (2 * dir)]);
        }
        if(Globals.getPieceByLocation(this.x ,this.y + dir) == null) {
            result.push([this.x, this.y + dir]);
        }
        piece = Globals.getPieceByLocation(this.x - 1, this.y + dir);
        if(piece != null && piece.light != this.light) result.push([this.x - 1, this.y + dir]);
        piece = Globals.getPieceByLocation(this.x + 1, this.y + dir);
        if(piece != null && piece.light != this.light) result.push([this.x + 1, this.y + dir]);
        return result;
    }

    displayAvailableMoves(context: CanvasRenderingContext2D) {
        var available = this.getAvailableMoves();
        context.fillStyle = '#0F0';
        for(var a of available) {
            context.fillRect(a[0] * this.bounds.w, a[1] * this.bounds.h, this.bounds.w, this.bounds.h);
            context.strokeStyle = '#0A0';
            context.strokeRect(a[0] * this.bounds.w, a[1] * this.bounds.h, this.bounds.w, this.bounds.h);
        }
    }

    onHover(context: CanvasRenderingContext2D) {
        context.lineWidth = 1;
        context.strokeStyle = '#000';
        context.strokeRect(this.bounds.x ,this.bounds.y, this.bounds.w, this.bounds.h);
    }

    wasCaptured() {
        this.hidden = true;
        this.captured = true;
        Globals.setPieceAtLocation(this.x, this.y, null);
    }

    remove() {
        // use when wanting to simply remove/delete piece
        // wasCaptured will eventually keep track of score
        this.hidden = true;
        this.captured = true;
        Globals.setPieceAtLocation(this.x, this.y, null);
    }

    /// use indices from the 8x8 board
    moveTo(xi: number, yi: number): boolean {
        var available = this.getAvailableMoves();
        var that = this;
        this.bounds.x = this.x * this.bounds.w;
        this.bounds.y = this.y * this.bounds.h;
        var result = false;
        available.forEach(function (value:[number, number], i, arr) {
            if (value[0] == xi && value[1] == yi) {
                if (Globals.getPieceByLocation(xi, yi) != null) {
                    var piece = Globals.getPieceByLocation(xi, yi);
                    if (piece.light != that.light) {
                        piece.wasCaptured();
                    }
                }
                Globals.setPieceAtLocation(that.x, that.y, null);
                that.x = xi;
                that.y = yi;
                that.bounds.x = xi * that.bounds.w;
                that.bounds.y = yi * that.bounds.h;
                Globals.setPieceAtLocation(that.x, that.y, that);
                that.firstMove = false;
                result = true;
            }
        });
        return result;
    }

    checkForCheckedKing(result: [number, number][]): [number, number][] {
        var king = this.light ? Globals.lightKing : Globals.darkKing;
        if(king != null && king.checked) {
            var newResult = [];
            for(var move of result) {
                Globals.setPieceAtLocation(this.x, this.y, null);
                var tmp = Globals.getPieceByLocation(move[0], move[1]);
                Globals.setPieceAtLocation(move[0], move[1], this);
                if(!king.isChecked()) {
                    newResult.push(move);
                }
                Globals.setPieceAtLocation(move[0], move[1], tmp);
                Globals.setPieceAtLocation(this.x, this.y, this)
            }
            return newResult;
        }
        return result;
    }
}