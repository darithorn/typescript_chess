import * as Game from './piece';


export const SCREEN_WIDTH = 512;
export const SCREEN_HEIGHT = 512;
export const PIECE_WIDTH = SCREEN_WIDTH / 8.0;
export const PIECE_HEIGHT = SCREEN_HEIGHT / 8.0;
export var pauseGame = false;
export var isLightTurn = true;
export var lightKing = null;
export var darkKing = null;
export var context: CanvasRenderingContext2D;
export var board: Array<Array<Game.Piece>> = [
    [ null, null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null, null ],
];
export var hoveredPiece: Game.Piece = null;
export var selectedPiece: Game.Piece = null;
export var draggingPiece: Game.Piece = null;

export function getPieceByLocation(xi, yi): Game.Piece {
    if(xi > -1 && xi < 8 && yi > -1 && yi < 8) return board[xi][yi];
    return undefined;
}

export function setPieceAtLocation(xi, yi, piece) {
    if(xi > -1 && xi < 8 && yi > -1 && yi < 8) {
        board[xi][yi] = piece;
    }
}