import {Piece} from './piece';
import * as Globals from './globals';

export class Knight extends Piece {
    constructor(boardIndex: [number, number], light: boolean) {
        super(boardIndex, light ? 'knight-light' : 'knight-dark');
    }

    getAvailableMoves(): Array<[number, number]> {
        var result = [];
        var piece = Globals.getPieceByLocation(this.x + 1, this.y - 2);
        if((piece != null && piece.light != this.light) || piece == null) result.push([this.x + 1, this.y - 2]);
        piece = Globals.getPieceByLocation(this.x - 1, this.y - 2);
        if((piece != null && piece.light != this.light) || piece == null) result.push([this.x - 1, this.y - 2]);
        piece = Globals.getPieceByLocation(this.x - 2, this.y - 1);
        if((piece != null && piece.light != this.light) || piece == null) result.push([this.x - 2, this.y - 1]);
        piece = Globals.getPieceByLocation(this.x - 2, this.y + 1);
        if((piece != null && piece.light != this.light) || piece == null) result.push([this.x - 2, this.y + 1]);
        
        piece = Globals.getPieceByLocation(this.x + 1, this.y + 2);
        if((piece != null && piece.light != this.light) || piece == null) result.push([this.x + 1, this.y + 2]);
        piece = Globals.getPieceByLocation(this.x - 1, this.y + 2);
        if((piece != null && piece.light != this.light) || piece == null) result.push([this.x - 1, this.y + 2]);
        piece = Globals.getPieceByLocation(this.x + 2, this.y - 1);
        if((piece != null && piece.light != this.light) || piece == null) result.push([this.x + 2, this.y - 1]);
        piece = Globals.getPieceByLocation(this.x + 2, this.y + 1);
        if((piece != null && piece.light != this.light) || piece == null) result.push([this.x + 2, this.y + 1]);
        return this.checkForCheckedKing(result);
    }
}