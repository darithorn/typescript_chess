import {Piece} from './piece';
import * as Globals from './globals';

export class Bishop extends Piece {
    constructor(boardIndex: [number, number], light: boolean) {
        super(boardIndex, light ? 'bishop-light' : 'bishop-dark');
    }

    getAvailableMoves(): Array<[number, number]> {
        var result = [];
        var upY = this.y;
        var downY = this.y;
        var stopUp = false;
        var stopDown = false;
        for(var x = this.x + 1; x < 8; x++) {
            var piece: Piece;
            if(!stopUp) {
                upY--;
                piece = Globals.board[x][upY];
                if(piece != null && piece.light == this.light) {
                    stopUp = true;
                } else {
                    result.push([x, upY]);
                    if (upY == 0 || Globals.board[x][upY]) {
                        stopUp = true;
                    }
                }
            }
            if(!stopDown) {
                downY++;
                piece = Globals.board[x][downY];
                if(piece != null && piece.light == this.light) {
                    stopDown = true;
                } else {
                    result.push([x, downY]);
                    if (downY == 8 || Globals.board[x][downY]) {
                        stopDown = true;
                    }
                }
            }
        }
        upY = this.y;
        downY = this.y;
        stopUp = false;
        stopDown = false;
        for(var x = this.x - 1; x > -1; x--) {
            var piece: Piece;
            if(!stopUp) {
                upY--;
                piece = Globals.board[x][upY];
                if(piece != null && piece.light == this.light) {
                    stopUp = true;
                } else {
                    result.push([x, upY]);
                    if (upY == 0 || Globals.board[x][upY]) {
                        stopUp = true;
                    }
                }
            }
            if(!stopDown) {
                downY++;
                piece = Globals.board[x][downY];
                if(piece != null && piece.light == this.light) {
                    stopDown = true;
                } else {
                    result.push([x, downY]);
                    if (downY == 8 || Globals.board[x][downY]) {
                        stopDown = true;
                    }
                }
            }
        }
        return this.checkForCheckedKing(result);
    }
}