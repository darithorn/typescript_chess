import * as Globals from './globals';
import {Piece} from './piece';
import {Bishop} from "./bishop";
import {Knight} from "./knight";
import {Queen} from "./queen";
import {Rook} from "./rook";

export class Pawn extends Piece {
    promote: boolean;
    enPassant: boolean;
    constructor(boardIndex: [number, number], light: boolean) {
        super(boardIndex, light ? 'pawn-light' : 'pawn-dark');
    }

    canPromote(): boolean {
        this.promote = this.light ? this.y == 0 : this.y == 7;
        return this.promote;
    }

    promoteTo(value: string) {
        var result: Piece = this;
        switch(value) {
            case 'bishop':
                result = new Bishop([this.x, this.y], this.light);
                break;
            case 'knight':
                result = new Knight([this.x, this.y], this.light);
                break;
            case 'rook':
                result = new Rook([this.x, this.y], this.light);
                break;
            case 'queen':
                result = new Queen([this.x, this.y], this.light);
                break;
            default:
                break;
        }
        this.remove();
        Globals.setPieceAtLocation(this.x, this.y, result);
    }

    getAvailableMoves(): Array<[number, number]> {
        var result = super.getAvailableMoves();
        var dir = this.light ? -1 : 1;
        var piece = Globals.getPieceByLocation(this.x + 1, this.y);
        if(piece instanceof Pawn) {
            var pawn = piece as Pawn;
            if(pawn.enPassant) {
                result.push([this.x + 1, this.y + dir]);
            }
        }
        piece = Globals.getPieceByLocation(this.x - 1, this.y);
        if(piece instanceof Pawn) {
            var pawn = piece as Pawn;
            if(pawn.enPassant) {
                result.push([this.x - 1, this.y + dir]);
            }
        }
        return result;
    }

    moveTo(xi: number, yi: number): boolean {
        var result = false;
        if(Math.abs(yi - this.y) == 2) this.enPassant = true; // enables en passant move for this pawn
        if(Math.abs(yi - this.y) == 1 && Math.abs(xi - this.x) == 1) {
            // if the piece moved diagonally for an en passant
            var dir = this.light ? 1 : -1; // reversed to obtain the pawn
            Globals.getPieceByLocation(xi, yi + dir).wasCaptured();
            Globals.setPieceAtLocation(this.x, this.y, null);
            this.x = xi;
            this.y = yi;
            this.bounds.x = xi * this.bounds.w;
            this.bounds.y = yi * this.bounds.h;
            Globals.setPieceAtLocation(this.x, this.y, this);
            this.firstMove = false;
            result = true;
        } else {
            result = super.moveTo(xi, yi);
        }
        this.canPromote();
        return result;
    }
}