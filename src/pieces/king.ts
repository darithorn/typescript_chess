import {Piece} from './piece';
import * as Globals from './globals';
import {Rook} from "./rook";

export class King extends Piece {
    checked: boolean = false;

    constructor(boardIndex: [number, number], light: boolean) {
        super(boardIndex, light ? 'king-light' : 'king-dark');
    }

    moveTo(xi: number, yi: number): boolean {
        if(this.firstMove) {
            // if we're moving to castling position
            var rook = null;
            if(xi == this.x - 2) {
                rook = Globals.getPieceByLocation(this.x - 4, yi);
                Globals.setPieceAtLocation(this.x - 1, yi, rook);
                Globals.setPieceAtLocation(this.x - 4, yi, null);
                Globals.setPieceAtLocation(this.x - 2, yi, this);
                Globals.setPieceAtLocation(this.x, yi, null);
                rook.x = this.x - 1;
                this.x = xi;
                this.bounds.x = xi * this.bounds.w;
                this.bounds.y = yi * this.bounds.h;
                return true;
            } else if(xi == this.x + 2) {
                rook = Globals.getPieceByLocation(this.x + 3, yi);
                Globals.setPieceAtLocation(this.x + 1, yi, rook);
                Globals.setPieceAtLocation(this.x + 3, yi, null);
                Globals.setPieceAtLocation(this.x + 2, yi, this);
                Globals.setPieceAtLocation(this.x, yi, null);
                rook.x = this.x + 1;
                this.x = xi;
                this.bounds.x = xi * this.bounds.w;
                this.bounds.y = yi * this.bounds.h;
                return true;
            }
        }
        return super.moveTo(xi, yi);
    }

    locationIsUnderAttack(xi, yi): boolean {
        for(var x = 0; x < 8; x++) {
            for(var y = 0; y < 8; y++) {
                var piece = Globals.getPieceByLocation(x, y);
                if(piece != null) {
                    if ((this.light && piece == Globals.darkKing) || (!this.light && piece == Globals.lightKing)) {
                        // This is a special case for Kings because
                        // kings are unable to get next to each other otherwise they would
                        // be able to capture each other
                        var distX = Math.abs(xi - piece.x);
                        var distY = Math.abs(yi - piece.y);
                        if (distX < 2 && distY < 2) return true;
                    } else if (piece != this && piece.light != this.light) {
                        var available = piece.getAvailableMoves();
                        for (var a of available) {
                            if (a[0] == xi && a[1] == yi) return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    isChecked(): boolean {
        this.checked = this.locationIsUnderAttack(this.x, this.y);
        return this.checked;
    }

    willBeChecked(xi, yi): boolean {
        if(xi >= 8 || xi < 0 || yi >= 8 || yi < 0) return true;
        var tmp = Globals.getPieceByLocation(xi as number, yi as number);
        Globals.setPieceAtLocation(this.x, this.y, null);
        Globals.setPieceAtLocation(xi, yi, this);
        var result = this.locationIsUnderAttack(xi, yi);
        Globals.setPieceAtLocation(xi, yi, tmp);
        Globals.setPieceAtLocation(this.x, this.y, this);
        return result;
    }

    getAvailableMoves(): Array<[number, number]> {
        var result = [];
        // Globals.board[this.x][this.y] = null;
        if(this.firstMove) {
            var leftOne = Globals.getPieceByLocation(this.x + 1, this.y);
            var leftTwo = Globals.getPieceByLocation(this.x + 2, this.y);
            var leftThree = Globals.getPieceByLocation(this.x + 3, this.y);
            if(leftOne == null && leftTwo == null && leftThree instanceof Rook) {
                result.push([this.x + 2, this.y]);
            }
            var rightOne = Globals.getPieceByLocation(this.x - 1, this.y);
            var rightTwo = Globals.getPieceByLocation(this.x - 2, this.y);
            var rightThree = Globals.getPieceByLocation(this.x - 3, this.y);
            var rightFour = Globals.getPieceByLocation(this.x - 4, this.y);
            if(rightOne == null && rightTwo == null && rightThree == null && rightFour instanceof Rook) {
                result.push([this.x - 2, this.y]);
            }
        }

        var tmpX = this.x - 1;
        var tmpY = this.y;
        var piece = Globals.getPieceByLocation(tmpX, tmpY);
        if(((piece != null && piece.light != this.light) || piece == null) &&
            !this.willBeChecked(tmpX, tmpY)) result.push([tmpX, tmpY]);

        tmpX = this.x + 1;
        tmpY = this.y;
        piece = Globals.getPieceByLocation(tmpX, tmpY);
        if(((piece != null && piece.light != this.light) || piece == null) &&
            !this.willBeChecked(tmpX, tmpY)) result.push([tmpX, tmpY]);

        tmpX = this.x;
        tmpY = this.y - 1;
        piece = Globals.getPieceByLocation(tmpX, tmpY);
        if(((piece != null && piece.light != this.light) || piece == null) &&
            !this.willBeChecked(tmpX, tmpY)) result.push([tmpX, tmpY]);

        tmpX = this.x;
        tmpY = this.y + 1;
        piece = Globals.getPieceByLocation(tmpX, tmpY);
        if(((piece != null && piece.light != this.light) || piece == null) &&
            !this.willBeChecked(tmpX, tmpY)) result.push([tmpX, tmpY]);

        tmpX = this.x - 1;
        tmpY = this.y + 1;
        piece = Globals.getPieceByLocation(tmpX, tmpY);
        if(((piece != null && piece.light != this.light) || piece == null) &&
            !this.willBeChecked(tmpX, tmpY)) result.push([tmpX, tmpY]);

        tmpX = this.x + 1;
        tmpY = this.y + 1;
        piece = Globals.getPieceByLocation(tmpX, tmpY);
        if(((piece != null && piece.light != this.light) || piece == null) &&
            !this.willBeChecked(tmpX, tmpY)) result.push([tmpX, tmpY]);

        tmpX = this.x - 1;
        tmpY = this.y - 1;
        piece = Globals.getPieceByLocation(tmpX, tmpY);
        if(((piece != null && piece.light != this.light) || piece == null) &&
            !this.willBeChecked(tmpX, tmpY)) result.push([tmpX, tmpY]);

        tmpX = this.x + 1;
        tmpY = this.y - 1;
        piece = Globals.getPieceByLocation(tmpX, tmpY);
        if(((piece != null && piece.light != this.light) || piece == null) &&
            !this.willBeChecked(tmpX, tmpY)) result.push([tmpX, tmpY]);

        return result;
    }
}