import {Piece} from './piece';
import * as Globals from './globals';
import {Rook} from "./rook";
import {Bishop} from "./bishop";

export class Queen extends Piece {
    constructor(boardIndex: [number, number], light: boolean) {
        super(boardIndex, light ? 'queen-light' : 'queen-dark');
    }

    getAvailableMoves(): Array<[number, number]> {
        var rook = new Rook(this.boardIndex, this.light);
        var bishop = new Bishop(this.boardIndex, this.light);
        var result = rook.getAvailableMoves().concat(bishop.getAvailableMoves());
        rook.remove();
        bishop.remove();
        Globals.board[this.x][this.y] = this;
        return result;
    }
}