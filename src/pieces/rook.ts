import * as Globals from './globals';
import {Piece} from "../piece";

export class Rook extends Piece {
    constructor(boardIndex: [number, number], light: boolean) {
        super(boardIndex, light ? 'rook-light' : 'rook-dark');
    }

    getAvailableMoves(): Array<[number, number]> {
        var result = [];
        for(var i = this.y + 1; i < 8; i += 1) {
            var piece = Globals.getPieceByLocation(this.x, i);
            // if(piece != null && piece.light == this.light) break;
            if(piece != null) {
                if(piece.light == this.light) break;
                result.push([this.x, i]);
                break;
            } else {
                result.push([this.x, i]);
            }
            // if (Globals.getPieceByLocation(this.x, i) != null) {
            //     break;
            // }
        }
        for(var i = this.y - 1; i > -1; i -= 1) {
            var piece = Globals.getPieceByLocation(this.x, i);
            if(piece != null) {
                if(piece.light == this.light) break;
                result.push([this.x, i]);
                break;
            } else {
                result.push([this.x, i]);
            }
            // if (Globals.getPieceByLocation(this.x, i) != null) {
            //     break;
            // }
        }
        for(var i = this.x + 1; i < 8; i += 1) {
            var piece = Globals.getPieceByLocation(i, this.y);
            if(piece != null) {
                if(piece.light == this.light) break;
                result.push([i, this.y]);
                break;
            } else {
                result.push([i, this.y]);
            }
            // if (Globals.getPieceByLocation(i, this.y) != null) {
            //     break;
            // }
        }
        for(var i = this.x - 1; i > -1; i -= 1) {
            var piece = Globals.getPieceByLocation(i, this.y);
            if(piece != null) {
                if(piece.light == this.light) break;
                result.push([i, this.y]);
                break;
            } else {
                result.push([i, this.y]);
            }
            // if (Globals.getPieceByLocation(i, this.y) != null) {
            //     break;
            // }
        }
        return this.checkForCheckedKing(result);
    }
}