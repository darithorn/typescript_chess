export class Rect {
    x: number;
    y: number;
    w: number;
    h: number;
    constructor(x, y, w, h) {
        this.x = x; this.y = y;
        this.w = w; this.h = h;
    }

    containsPoint(x, y): boolean {
        return x >= this.x && y >= this.y && x <= (this.x + this.w) && y <= (this.y + this.h);
    }
}