import {Rook} from './pieces/rook';
import {Bishop} from './pieces/bishop';
import * as Game from './piece';
import * as Globals from './globals';
import {Knight} from "./pieces/knight";
import {Queen} from "./pieces/queen";
import {King} from "./pieces/king";
import {Pawn} from './pieces/pawn';
import {darkKing} from "./globals";
import {lightKing} from "./globals";
import {pauseGame} from "./globals";

function setupBoard() {
    for(var x = 0; x < 8; x++) {
        new Pawn([x, 1], false);
        new Pawn([x, 6], true);
    }
    // // Light
    new Rook([0, 7], true);
    new Rook([7, 7], true);
    new Bishop([1, 7], true);
    new Bishop([6, 7], true);
    new Knight([2, 7], true);
    new Knight([5, 7], true);
    new Queen([3, 7], true);
    Globals.lightKing = new King([4, 7], true);
    //
    new Rook([0, 0], false);
    new Rook([7, 0], false);
    new Bishop([1, 0], false);
    new Bishop([6, 0], false);
    new Knight([2, 0], false);
    new Knight([5, 0], false);
    new Queen([3, 0], false);
    Globals.darkKing = new King([4, 0], false);
}

function draw() {
    // draw the board
    let rectWidth = Globals.SCREEN_WIDTH / 8.0;
    let rectHeight = Globals.SCREEN_HEIGHT / 8.0;
    var fill = false;
    for(var x = 0; x < 8; x++) {
        for(var y = 0; y < 8; y++) {
            if(fill) Globals.context.fillStyle = '#CD853F';
            else Globals.context.fillStyle = '#FFBF00';
            Globals.context.fillRect(x * rectWidth, y * rectHeight, rectWidth, rectHeight);
            fill = !fill;
        }
        fill = !fill;
    }
    if(Globals.selectedPiece != null) {
        Globals.selectedPiece.onHover(Globals.context);
        Globals.selectedPiece.displayAvailableMoves(Globals.context);
    }
    if(Globals.hoveredPiece != null) Globals.hoveredPiece.onHover(Globals.context);
    if(Globals.draggingPiece != null) Globals.draggingPiece.drawPixel(Globals.context);
    for(var x = 0; x < 8; x++) {
        for(var y = 0; y < 8; y++) {
            var p = Globals.board[x][y];
            if(p != null) {
                if(!p.hidden) p.draw(Globals.context);
            }
        }
    }
}

function getContainedPiece(xi, yi) {
    for(var x = 0; x < 8; x++) {
        for(var y = 0; y < 8; y++) {
            var p = Globals.board[x][y];
            if(p != null) {
                if(!p.hidden && !p.captured && p.bounds.containsPoint(xi, yi)) {
                    return p;
                }
            }
        }
    }
    return null;
}

function nextTurn() {
    Globals.selectedPiece = null;
    Globals.isLightTurn = !Globals.isLightTurn;
    for(var x = 0; x < 8; x++) {
        for(var y = 0; y < 8; y++) {
            var piece = Globals.getPieceByLocation(x, y);
            if(piece instanceof Pawn && ((piece.light && Globals.isLightTurn) || (!piece.light && !Globals.isLightTurn))) {
                var pawn = piece as Pawn;
                pawn.enPassant = false;
            }
        }
    }
    document.getElementById('turn-info').innerHTML = Globals.isLightTurn ? 'White Turn' : 'Black Turn';
}

export function main() {
    var canvas = document.getElementById('main-canvas') as HTMLCanvasElement;
    canvas.width = Globals.SCREEN_WIDTH;
    canvas.height = Globals.SCREEN_HEIGHT;
    var mouseDown = false;
    var clickedX:number, clickedY:number;
    canvas.addEventListener('mousemove', function(m) {
        if(Globals.pauseGame) return;
        if(Globals.draggingPiece == null) {
            // don't want to outline pieces while dragging
            Globals.hoveredPiece = getContainedPiece(m.x, m.y);
        } else {
            Globals.draggingPiece.bounds.x = m.x + clickedX;
            Globals.draggingPiece.bounds.y = m.y + clickedY;
        }
        if(Globals.selectedPiece != null && mouseDown &&
            ((Globals.isLightTurn && Globals.selectedPiece.light) || (!Globals.isLightTurn && !Globals.selectedPiece.light))) {
            Globals.draggingPiece = Globals.selectedPiece;
            Globals.draggingPiece.hidden = true;
        }
    });
    canvas.addEventListener('mousedown', function(m) {
        if(Globals.pauseGame) return;
        mouseDown = true;
        Globals.selectedPiece = getContainedPiece(m.x, m.y);
        if(Globals.selectedPiece != null) {
            if(((Globals.selectedPiece.light && !Globals.isLightTurn) ||
                (!Globals.selectedPiece.light && Globals.isLightTurn))) {
                // if we tried to select a piece that wasn't the correct color
                // i.e if it's white's turn and we tried to select a black piece
                Globals.selectedPiece = null;
            }
            // else if((Globals.isLightTurn && (lightKing != null && lightKing.checked)) || (!Globals.isLightTurn && (darkKing != null && darkKing.checked)) &&
            //             typeof(Globals.selectedPiece) != typeof(King)) {
            //     // if the player's king is in check and tried to select a piece that's not the King
            //     Globals.selectedPiece = null;
            // }
            else {
                clickedX = Globals.selectedPiece.bounds.x - m.x;
                clickedY = Globals.selectedPiece.bounds.y - m.y;
            }
        }
    });
    canvas.addEventListener('mouseup', function(m) {
        if(Globals.pauseGame) return;
        mouseDown = false;
        if(Globals.draggingPiece != null) {
            document.getElementById('check-info').innerHTML = '';
            var destX = Math.floor(m.x / Globals.draggingPiece.bounds.w);
            var destY = Math.floor(m.y / Globals.draggingPiece.bounds.h);
            var success = Globals.draggingPiece.moveTo(destX, destY);
            // if not dropped in same place
            if(success) {
                if(Globals.draggingPiece instanceof Pawn && (Globals.draggingPiece as Pawn).promote) {
                    Globals.pauseGame = true;
                    document.getElementById('promotion').removeAttribute('hidden');
                    var tmp = Globals.draggingPiece;
                    document.getElementById('promotion').addEventListener('change', function(s) {
                        if(s.target.selectedIndex !== 0) {
                            (tmp as Pawn).promoteTo(s.target.value);
                            nextTurn();
                            Globals.pauseGame = false;
                            document.getElementById('promotion').setAttribute('hidden', 'true');
                        }
                    });
                } else {
                    nextTurn();
                }
                var king = Globals.isLightTurn ? lightKing : darkKing;
                if(king != null) {
                    var noun = Globals.isLightTurn ? 'White' : 'Black';
                    if(king.isChecked()) {
                        document.getElementById('check-info').innerHTML = noun + ' King Checked';
                        if(king.getAvailableMoves().length == 0) {
                            var win = Globals.isLightTurn ? 'Black Wins' : 'White Win';
                            document.getElementById('check-info').innerHTML = noun + ' King Checkmate';
                            document.getElementById('win-info').innerHTML = win;
                            Globals.pauseGame = true;
                        }
                    }
                }
            }
            Globals.draggingPiece.hidden = false;
            Globals.draggingPiece = null;
        }
    });
    Globals.context = canvas.getContext('2d');
    setupBoard();
    window.setInterval(draw);
}